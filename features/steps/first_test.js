require('chromedriver');
const { Given, When, Then, setDefaultTimeout } = require("@cucumber/cucumber");
const {Builder, By, Key, until} = require('selenium-webdriver');
const webdriver = require('selenium-webdriver');
const expect = require('expect');
const moment = require('moment');
const https = require('https');
const axios = require('axios');

const jsonQuery = require('./resources/first_call.json')

// const assert = require("assert").strict
const sleep = require('sleep');
// let kabanaResponse;
let kabanaJson = jsonQuery;

setDefaultTimeout(30 * 1000);
let browser = new webdriver.Builder().forBrowser('chrome').build();


Given('the user is logged in', async function () {
    await browser.manage().window().maximize();

    await browser.get('https://saemobilus-dev.sae.org');
    await browser.findElement(By.className('m-login-box')).click();
    await browser.findElement(By.id('userid')).sendKeys('autotest-a2m');
    await browser.findElement(By.id('password')).sendKeys('autotest-a2m');
    await browser.findElement(By.xpath('//*[@id="subLogin"]/form/div[3]/button')).click();

    await browser.findElement(By.className('fa-caret-down')).click();
    await browser.findElement(By.linkText('Additional Subscription Login')).click();
    await browser.findElement(By.id('userid')).sendKeys('autotest-n2z');
    await browser.findElement(By.id('password')).sendKeys('autotest-n2z');
    await browser.findElement(By.xpath('//*[@id="subLogin"]/form/div[3]/button')).click();
});

When('they search for {string}', async function (searchWord) {
    await browser.findElement(By.id('advSearchTxt0')).sendKeys(searchWord);
    await browser.findElement(By.linkText('Search')).click();
});

When('results are shown', async function () {
    const results = (await browser.findElement(By.id('all-search-results'))).isDisplayed();
    expect(await results).toBe(true);
    browser.close();
});

Then('Kabana logs the activity', async function () {
    const now = new Date(Date.now());
    const timeRangeStart = moment.utc(now).subtract(20, 'seconds').format();
    const timeRangeEnd = moment.utc(now).add(2, 'minutes').format()

    const elsasticSearch = 'https://es.dev.sae.cloud:9200/saedb.usagetrackingenhancedevents/_search'
    
    const httpsAgent = new https.Agent({
        rejectUnauthorized: false,
    });
    
    kabanaJson.query.bool.filter[1].range.created.gte = timeRangeStart
    kabanaJson.query.bool.filter[1].range.created.lte = timeRangeEnd

    await axios.post(elsasticSearch, kabanaJson, {
        httpsAgent: httpsAgent,
        auth: {
            username: 'saeuser',
            password: 'testpassword'
        }
    })
    .then((response) => {
        this.kabanaResponse = response.data
    }, (error) => {
        console.log('You got an error, man...')
        console.log(error);
    });
});

Then('the json contains user data', async function () {
    expect(this.kabanaResponse.hits.hits[0]._source.sae.subscription.title).toBe('Automated Testing > Collections A to M');
    expect(this.kabanaResponse.hits.hits[1]._source.sae.subscription.title).toBe('Automated Testing > Collections N to Z');
    expect(this.kabanaResponse.hits.hits[0]._source.event.action).toBe('login_subscription');
    expect(this.kabanaResponse.hits.hits[1]._source.event.action).toBe('login_subscription');
});