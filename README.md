# Usage Tracking Automated Testing

This repo contains automated tests to verify stability of the [usage-tracking-service](https://gitlab.com/sae_devops/usage-tracking-service).

The following languages/tools are used in the tests

  - Cucumber - testing framework
  - Selenium - for automating UI interactions
  - Axios - for API calls to verify Elastic/Kabana results

### Installation and Usage

The tests were setup using [Node.js](https://nodejs.org/) v14.15+.

Install the dependencies and devDependencies and run the tests.

```sh
$ npm install -d
$ npm test
```